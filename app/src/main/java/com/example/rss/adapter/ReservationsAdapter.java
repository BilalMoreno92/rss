package com.example.rss.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rss.R;
import com.example.rss.model.Reservation;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by paco on 6/02/18.
 */

public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {
    private ArrayList<Reservation> reservations;

    public ReservationsAdapter(){
        this.reservations = new ArrayList<>();
    }

    public void setRepos(ArrayList<Reservation> repos) {
        reservations = repos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvDate) TextView date;
        @BindView(R.id.tvStart) TextView start;
        @BindView(R.id.tvEnd) TextView end;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        // Inflate the custom layout
        LayoutInflater inflater = LayoutInflater.from(context);
        View reservationView = inflater.inflate(R.layout.item_view, parent, false);

        // Return a new holder instance
        return new ViewHolder(reservationView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Reservation reservation = reservations.get(position);

        holder.date.setText(reservation.getDate());
        holder.start.setText(reservation.getStart());
        holder.end.setText(reservation.getEnd());
    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }

    public int getId(int position){

        return this.reservations.get(position).getId();
    }

    public Reservation getAt(int position){
        Reservation reservation;
        reservation = this.reservations.get(position);
        return reservation;
    }

    public void add(Reservation reservation) {
        this.reservations.add(reservation);
        notifyItemInserted(reservations.size() - 1);
        notifyItemRangeChanged(0, reservations.size() - 1);
    }

    public void modifyAt(Reservation reservation, int position) {
        this.reservations.set(position, reservation);
        notifyItemChanged(position);
    }

    public void removeAt(int position) {
        this.reservations.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, reservations.size() - 1);
    }
}
