package com.example.rss.network;

import com.example.rss.model.Email;
import com.example.rss.model.LoginResponse;
import com.example.rss.model.LogoutResponse;
import com.example.rss.model.RegisterResponse;
import com.example.rss.model.Reservation;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiTokenService {
    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password);

    //@POST("api/register")
    //Call<RegisterResponse>register(@Body User user);

    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> login(
            @Field("email") String email,
            @Field("password") String password);
    //@POST("api/login")
    //Call<LoginResponse>login(@Body User user);

    @POST("api/logout")
    Call<LogoutResponse> logout(
            //@Header("Authorization") String token
    );

    @GET("api/reservations")
    Call<ArrayList<Reservation>> getReservations(
            //@Header("Authorization") String token
    );

    @POST("api/reservations")
    Call<Reservation> createReservation(
            //@Header("Authorization") String token,
            @Body Reservation reservation);

    @PUT("api/reservations/{id}")
    Call<Reservation> updateReservation(
            //@Header("Authorization") String token,
            @Body Reservation reservation,
            @Path("id") int id);

    @DELETE("api/reservations/{id}")
    Call<ResponseBody> deleteReservation(
            //@Header("Authorization") String token,
            @Path("id") int id);

    @POST("api/email")
    Call<ResponseBody> sendEmail(@Body Email email);
}

