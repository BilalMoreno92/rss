package com.example.rss.model;

/**
 * Created by paco
 */

import java.io.Serializable;

public class Reservation implements Serializable {
    private int id;
    private String date;
    private String start;
    private String end;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getStart() {
        return start;
    }
    public void setStart(String start) {
        this.start = start;
    }
    public String getEnd() {
        return end;
    }
    public void setEnd(String end) {
        this.end = end;
    }
    public Reservation(int id, String date, String start, String end) {
        super();
        this.id = id;
        this.date = date;
        this.start = start;
        this.end = end;
    }
    public Reservation(String date, String start, String end) {
        super();
        this.date = date;
        this.start = start;
        this.end = end;
    }

    public Reservation() {}

    @Override
    public String toString() {
        return  date + '\n' + end;
    }
}
