package com.example.rss.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.DatePicker;

import com.example.rss.R;
import com.example.rss.model.Reservation;
import com.example.rss.network.ApiTokenRestClient;
import com.example.rss.util.SharedPreferencesManager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener, Callback<Reservation> {
    public static final int OK = 1;

    @BindView(R.id.dateReservation) EditText dateReservation;
    @BindView(R.id.startReservation) EditText startReservation;
    @BindView(R.id.endReservation) EditText endReservation;
    @BindView(R.id.accept) Button accept;
    @BindView(R.id.cancel) Button cancel;

    ProgressDialog progreso;
    Reservation r;
    SharedPreferencesManager preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);

        accept.setOnClickListener(this);
        cancel.setOnClickListener(this);

        Intent i = getIntent();
        r = (Reservation) i.getSerializableExtra("reservation");
        dateReservation.setText(r.getDate());
        SimpleDateFormat inDateFormat = new SimpleDateFormat("H:mm:ss");
        SimpleDateFormat outDateFormat = new SimpleDateFormat("H:mm");
        try {
            startReservation.setText(outDateFormat.format(inDateFormat.parse(r.getStart())));
            endReservation.setText(outDateFormat.format(inDateFormat.parse(r.getEnd())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.dateReservation)
    public void selectDate(View view) {
        DialogFragment datePicker = new DatePickerFragment();
        ((DatePickerFragment) datePicker).setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        ((DatePickerFragment) datePicker).setEditText(dateReservation);
        datePicker.show(getSupportFragmentManager(), "datePicker");
    }

    @OnClick({R.id.startReservation, R.id.endReservation})
    public void selectTime(View view) {
        DialogFragment timePicker = new TimePickerFragment();
        ((TimePickerFragment) timePicker).setDateFormat(new SimpleDateFormat("H:mm"));
        switch (view.getId()) {
            case R.id.startReservation:
                ((TimePickerFragment) timePicker).setEditText(startReservation);
                break;
            case R.id.endReservation:
                ((TimePickerFragment) timePicker).setEditText(endReservation);
                break;
        }
        timePicker.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onClick(View v) {
        String n, l, e;

        hideSoftKeyboard();
        if (v == accept) {
            n = dateReservation.getText().toString();
            l = startReservation.getText().toString();
            e = endReservation.getText().toString();
            if (n.isEmpty() || l.isEmpty() || e.isEmpty())
                Toast.makeText(this, "Please, fill all fields.", Toast.LENGTH_SHORT).show();
            else {
                r.setDate(n);
                r.setStart(l);
                r.setEnd(e);
                connection(r);
            }
        }
        if (v == cancel)
            finish();
    }

    private void connection(Reservation s) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Reservation> call = ApiRestClient.getInstance().updateReservation("Bearer " + preferences.getToken(), r, r.getId());
        Call<Reservation> call = ApiTokenRestClient.getInstance(preferences.getToken()).updateReservation(s, s.getId());
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Reservation> call, Response<Reservation> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            Reservation reservation = response.body();
            Intent i = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("id", reservation.getId());
            bundle.putString("date", reservation.getDate());
            bundle.putString("start", reservation.getStart());
            bundle.putString("end", reservation.getEnd());
            i.putExtras(bundle);
            setResult(OK, i);
            finish();
            showMessage("Modified reservation ok");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<Reservation> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}