package com.example.rss.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat dateFormat;
    private EditText editText;

    public void setDateFormat(SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int hour;
        int minute;
        if (!editText.getText().toString().isEmpty()) {

            try {
                calendar.setTime(dateFormat.parse(editText.getText().toString()));
            } catch (ParseException e) {
                Toast.makeText(getContext(), "Formato no válido", Toast.LENGTH_SHORT).show();
            }
        }

        // Use the current date as the default date in the picker
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        dateFormat.setCalendar(calendar);
        editText.setText(dateFormat.format(calendar.getTime()));
    }
}
