package com.example.rss.ui;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rss.R;
import com.example.rss.model.Reservation;
import com.example.rss.network.ApiTokenRestClient;
import com.example.rss.util.SharedPreferencesManager;

import java.io.IOException;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity implements Callback<Reservation> {
    public static final int OK = 1;

    @BindView(R.id.dateReservation) EditText dateReservation;
    @BindView(R.id.startReservation) EditText startReservation;
    @BindView(R.id.endReservation) EditText endReservation;

    ProgressDialog progreso;
    SharedPreferencesManager preferences;

    @OnClick(R.id.dateReservation)
    public void selectDate(View view) {
        DialogFragment datePicker = new DatePickerFragment();
        ((DatePickerFragment) datePicker).setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        ((DatePickerFragment) datePicker).setEditText(dateReservation);
        datePicker.show(getSupportFragmentManager(), "datePicker");
    }

    @OnClick({R.id.startReservation, R.id.endReservation})
    public void selectTime(View view) {
        DialogFragment timePicker = new TimePickerFragment();
        ((TimePickerFragment) timePicker).setDateFormat(new SimpleDateFormat("H:mm"));
        switch (view.getId()) {
            case R.id.startReservation:
                ((TimePickerFragment) timePicker).setEditText(startReservation);
                break;
            case R.id.endReservation:
                ((TimePickerFragment) timePicker).setEditText(endReservation);
                break;
        }
        timePicker.show(getSupportFragmentManager(), "datePicker");
    }

    @OnClick(R.id.accept)
    public void clickAccept(View view){
        String d, s, e;
        Reservation r;

        hideSoftKeyboard();
        d = dateReservation.getText().toString();
        s = startReservation.getText().toString();
        e = endReservation.getText().toString();
        if (d.isEmpty() || s.isEmpty() || e.isEmpty())
            Toast.makeText(this, "Please, fill all fields", Toast.LENGTH_SHORT).show();
        else {
            r = new Reservation(d, s , e);
            connection(r);
        }
    }

    @OnClick(R.id.cancel)
    public void clickCancel(View view){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);
    }

    private void connection(Reservation s) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Reservation> call = ApiRestClient.getInstance().createReservation("Bearer " + preferences.getToken(), r);
        Call<Reservation> call = ApiTokenRestClient.getInstance(preferences.getToken()).createReservation(s);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Reservation> call, Response<Reservation> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            Reservation reservation = response.body();
            Intent i = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("id", reservation.getId());
            bundle.putString("date", reservation.getDate());
            bundle.putString("start", reservation.getStart());
            bundle.putString("end", reservation.getEnd());
            i.putExtras(bundle);
            setResult(OK, i);
            finish();
            showMessage("Added reservation ok");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<Reservation> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

}

